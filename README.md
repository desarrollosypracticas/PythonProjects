Ejercicios de Python para sus diferentes plataformas.


![alt tag](https://cdn-images-1.medium.com/max/1600/1*PPIp7twJJUknfohZqtL8pQ.png)


Varios ejemplos de código hechos en python (Hechos en Geany).


## Lista de Ejercicios
| Nombre del Ejercicio          | Descripcion   |
| ------------------------------|---------------|
| python001_holamundo           | Imprime texto en pantalla.
| python002_sumatorio           | Sumatoria de las raices de dos números.
 





