from math import * #Importa las funciones matematicas

#Pedir limites inferior y superior.
a=int(raw_input('Limite_inferior:'))
while a<0:
        print 'No puede ser negativo'   #Impresion de texto
        a=int(raw_input('Limite inferior:')) #pide valor para a, con un texto y lo convierte a entero

b=int(raw_input('Limite superior:'))
while b<a:
        print 'No puede ser menor que %d'%a #Imprime con un valor de la variable a
        b=int(raw_input('Limite superior:'))

#Calcular el sumatorio de la raiz cuadrada de i para i entre a y b
s=0.0
for i in range(a,b+1): #for
    s +=sqrt(i)  #saca la raiz de i

#Mostrar el resultado
print 'Sumatoria de las raices',
print 'de %d a %d: %f'%(a,b,s)
